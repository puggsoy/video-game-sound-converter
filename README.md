#Video Game Sound Converter

Video Game Sound Converter (a.k.a. VGSC) is a wrapper for vgmstream, a program that can convert various video game sound formats to WAV. It allows multiple file conversion, and also features a minimal file selection UI.

##Future features

* Allow optional vgmstream arguments
* Maybe a proper UI, if I can find (or make) a good library for it